﻿@tfs:https://teamkyckr.visualstudio.com/Kyckr-Main
Feature: CompanySearch
	As a company user
	I want to be able to search Companies in a given country

@story:1158
@Feature CompanySearch
Scenario Outline: Search by country
	Given Given user "kieran\.osullivan@gbrdirect\.com" accesses API
	And And enters "somePassword" as the password	
	And And company name "kyckr" is entered
	And And country ISO code <countryIso> is entered
	And And terms and conditions are accepted
	When user searches for "Kyckr"
	Then the result list should include "Kyckr"
Examples: 
| countryIso |
| US         |
| DE         |
| FR         |
| AU         |