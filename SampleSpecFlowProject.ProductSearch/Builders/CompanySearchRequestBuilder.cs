﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleSpecFlowProject.ProductSearch.Personas;

namespace SampleSpecFlowProject.ProductSearch.Builders
{
    public class CompanySearchRequestBuilder
    {
        private string userName;
        private string password;
        private string companyName;
        private string countryCode;
        private bool termsAndConditionsAccepted;

        public static CompanySearchRequestBuilder For<T>() where T : WellKnownUser, new()
        {
            var wellKnownUser = new T();
            return new CompanySearchRequestBuilder()
                .WithUserName(wellKnownUser.Name)
                .WithPassword(wellKnownUser.Password);
        }

        public CompanySearchRequestBuilder WithUserName(string userName)
        {
            this.userName = userName;
            return this;
        }

        public CompanySearchRequestBuilder WithPassword(string password)
        {
            this.password = password;
            return this;
        }
        public CompanySearchRequestBuilder WithComanyName(string companyName)
        {
            this.companyName = companyName;
            return this;
        }
        public CompanySearchRequestBuilder WithCountryCode(string countryCode)
        {
            this.countryCode = countryCode;
            return this;
        }

        public CompanySearchRequestBuilder WithTermsAndConditionsAccepted()
        {
            this.termsAndConditionsAccepted = true;
            return this;
        }

        public CompanySearchRequest Build()
        {
            return new CompanySearchRequest(userName,
                password,
                companyName,
                countryCode,
                termsAndConditionsAccepted);
        }
    }
}
