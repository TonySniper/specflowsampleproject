﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSpecFlowProject.ProductSearch.Builders
{
    public class CompanySearchRequest
    {
        public CompanySearchRequest(string userName,
            string password,
            string companyName,
            string countryCode,
            bool termsAndConditionsAccepted = false)
        {
            UserName = userName;
            Password = password;
            CompanyName = companyName;
            CountryCode = countryCode;
            TermsAndConditionsAccepted = termsAndConditionsAccepted;
        }

        public string UserName { get; }
        public string Password { get; }
        public string CompanyName { get; }
        public string CountryCode { get; }
        public bool TermsAndConditionsAccepted { get; }
    }
}
