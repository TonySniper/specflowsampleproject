﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSpecFlowProject.ProductSearch.Personas
{
    public class WellKnownUsers
    {
        public class BloombergUser : WellKnownUser
        {
            public override string Name => "BloombergAdmin";
            public override string Password => "SomePassword";
        }
    }
}
