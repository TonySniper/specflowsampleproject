﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSpecFlowProject.ProductSearch.Personas
{
    public abstract class WellKnownUser
    {
        public abstract string Name { get; }
        public abstract string Password { get; }
    }
}
