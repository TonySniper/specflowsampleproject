﻿using System;
using SampleSpecFlowProject.ProductSearch.Builders;
using TechTalk.SpecFlow;

namespace SampleSpecFlowProject.ProductSearch.Steps
{
    [Binding]
    public class CompanySearchSteps
    {
        readonly CompanySearchRequestBuilder requestBuilder;

        public CompanySearchSteps()
        {
            requestBuilder = new CompanySearchRequestBuilder();
        }

        [Given(@"Given user (.*) accesses API")]
        public void GivenUser(string user)
        {
            requestBuilder.WithUserName(user);
            //ScenarioContext.Current.Pending();
        }

        [Given(@"And enters (.*) as the password")]
        public void AndEntersThePassword(string password)
        {
            requestBuilder.WithUserName(password);
            //ScenarioContext.Current.Pending();
        }

        [Given(@"And country ISO code (.*) is entered")]
        public void GivenAndCountryISOCodeIsEntered(string countryCode)
        {
            requestBuilder.WithCountryCode(countryCode);
            //ScenarioContext.Current.Pending();
        }

        [Given(@"And company name ""(.*)"" is entered")]
        public void GivenAndCompanyNameIsEntered(string companyName)
        {
            requestBuilder.WithCountryCode(companyName);
           // ScenarioContext.Current.Pending();
        }

        [Given(@"And terms and conditions are accepted")]
        public void GivenAndTermsAndConditionsAreAccepted()
        {
            requestBuilder.WithTermsAndConditionsAccepted();
            //ScenarioContext.Current.Pending();
        }

        [When(@"user searches for (.*)")]
        public void WhenUserSearches(string companyName)
        {
            var request = requestBuilder.Build();
            //ScenarioContext.Current.Pending();
        }

        [Then(@"the result list should include (.*)")]
        public void ThenTheResultListShouldInclude(string companyName)
        {
            //ScenarioContext.Current.Pending();
        }

        //[Given(@"Given user ""(.*)"" accesses API")]
        //public void GivenGivenUserAccessesAPI(string user)
        //{
        //    requestBuilder.WithUserName(user);
        //    ScenarioContext.Current.Pending();
        //}

        //[Given(@"And enters ""(.*)"" as the password")]
        //public void GivenAndEntersAsThePassword(string password)
        //{
        //    requestBuilder.WithPassword(password);
        //    ScenarioContext.Current.Pending();
        //}

        //[Given(@"And company name ""(.*)"" is entered")]
        //public void GivenAndCompanyNameIsEntered(string p0)
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[Given(@"And country ISO code (.*) is entered")]
        //public void GivenAndCountryISOCodeUSIsEntered(string countryCode)
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[Given(@"And terms and conditions are accepted")]
        //public void GivenAndTermsAndConditionsAreAccepted()
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[When(@"user searches for ""(.*)""")]
        //public void WhenUserSearchesFor(string p0)
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[Then(@"the result list should include ""(.*)""")]
        //public void ThenTheResultListShouldInclude(string p0)
        //{
        //    ScenarioContext.Current.Pending();
        //}
    }
}
